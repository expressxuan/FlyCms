package com.flycms.framework.security.service;

import com.flycms.common.constant.Constants;
import com.flycms.common.utils.IdUtils;
import com.flycms.common.utils.ServletUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.ip.IpUtils;
import com.flycms.framework.redis.RedisCache;
import com.flycms.framework.security.LoginAdmin;
import com.flycms.modules.data.domain.dto.IpAddressDTO;
import com.flycms.modules.data.service.IIpAddressService;
import eu.bitwalker.useragentutils.UserAgent;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * token验证处理
 * 
 * @author kaifei sun
 */
@Component
public class TokenService
{
    // 令牌自定义标识
    @Value("${token.header}")
    private String header;

    // 令牌秘钥
    @Value("${token.secret}")
    private String secret;

    // 令牌有效期（默认30分钟）
    @Value("${token.expireTime}")
    private int expireTime;

    protected static final long MILLIS_SECOND = 1000;

    protected static final long MILLIS_MINUTE = 60 * MILLIS_SECOND;

    private static final Long MILLIS_MINUTE_TEN = 20 * 60 * 1000L;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private IIpAddressService ipAddressService;

    // 生成密钥
    private static final String SECRET = "WH#$%(!)(#*!()!KL<55$6><MQLMNQNQJQK EiLCJuYW1lIjoif>?N<:{LWPW";
    // Base64就是一种基于64个可打印字符来表示二进制数据的方法。可查看RFC2045～RFC2049，上面有MIME的详细规范。
    // signWith中，如果传递字符串，需要传递base64编码的字符串

    /**
     * 生成的密钥
     */
    private static final Key SIGN_KEY = generalSecretKey(SignatureAlgorithm.HS256, SECRET);

    /**
     * 生成key，用于生成token
     *
     * @param signatureAlgorithm 指定加密算法
     * @param secretKey          自定义秘钥，仅仅后台知道
     * @return Key
     */
    public static Key generalSecretKey(SignatureAlgorithm signatureAlgorithm, String secretKey) {
        // 在Java 8在java.util包下面实现了BASE64编解码API，API简单易懂
        byte[] base64EncodedSecretKey = null;
        try {
            base64EncodedSecretKey = Base64.getEncoder().encode(secretKey.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // 这样也是可以的 Key key = MacProvider.generateKey()
        if (base64EncodedSecretKey == null) {
            throw new IllegalArgumentException();
        }
        return new SecretKeySpec(base64EncodedSecretKey, signatureAlgorithm.getJcaName());
    }


    /**
     * 获取用户身份信息
     * 
     * @return 用户信息
     */
    public LoginAdmin getLoginAdmin(HttpServletRequest request)
    {
        // 获取请求携带的令牌
        String token = getToken(request);
        if (StrUtils.isNotEmpty(token))
        {
            Claims claims = parseToken(token);
            // 解析对应的权限以及用户信息
            String uuid = (String) claims.get(Constants.LOGIN_ADMIN_KEY);
            String userKey = getTokenKey(uuid);
            LoginAdmin user = redisCache.getCacheObject(userKey);
            return user;
        }
        return null;
    }

    /**
     * 设置用户身份信息
     */
    public void setLoginAdmin(LoginAdmin loginAdmin)
    {
        if (StrUtils.isNotNull(loginAdmin) && StrUtils.isNotEmpty(loginAdmin.getToken()))
        {
            refreshToken(loginAdmin);
        }
    }

    /**
     * 删除用户身份信息
     */
    public void delLoginAdmin(String token)
    {
        if (StrUtils.isNotEmpty(token))
        {
            String userKey = getTokenKey(token);
            redisCache.deleteObject(userKey);
        }
    }

    /**
     * 创建令牌
     * 
     * @param loginAdmin 用户信息
     * @return 令牌
     */
    public String createToken(LoginAdmin loginAdmin)
    {
        String token = IdUtils.fastUUID();
        loginAdmin.setToken(token);
        setUserAgent(loginAdmin);
        refreshToken(loginAdmin);

        Map<String, Object> claims = new HashMap<>();
        claims.put(Constants.LOGIN_ADMIN_KEY, token);
        return createToken(claims);
    }

    /**
     * 验证令牌有效期，相差不足20分钟，自动刷新缓存
     * 
     * @param loginAdmin 令牌
     * @return 令牌
     */
    public void verifyToken(LoginAdmin loginAdmin)
    {
        long expireTime = loginAdmin.getExpireTime();
        long currentTime = System.currentTimeMillis();
        if (expireTime - currentTime <= MILLIS_MINUTE_TEN)
        {
            refreshToken(loginAdmin);
        }
    }

    /**
     * 刷新令牌有效期
     * 
     * @param loginAdmin 登录信息
     */
    public void refreshToken(LoginAdmin loginAdmin)
    {
        loginAdmin.setLoginTime(System.currentTimeMillis());
        loginAdmin.setExpireTime(loginAdmin.getLoginTime() + expireTime * MILLIS_MINUTE);
        // 根据uuid将loginUser缓存
        String userKey = getTokenKey(loginAdmin.getToken());
        redisCache.setCacheObject(userKey, loginAdmin, expireTime, TimeUnit.MINUTES);
    }
    
    /**
     * 设置用户代理信息
     * 
     * @param loginAdmin 登录信息
     */
    public void setUserAgent(LoginAdmin loginAdmin)
    {
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        String ip = IpUtils.getIpAddr(ServletUtils.getRequest());
        loginAdmin.setIpaddr(ip);
        if(StrUtils.isboolIp(ip)){
            IpAddressDTO address=ipAddressService.findSearchIpAddress(ip);
            if(address != null){
                StringBuffer sb = new StringBuffer();
                if(!StrUtils.isEmpty(address.getProvince())){
                    sb.append(address.getProvince());
                }
                if(!StrUtils.isEmpty(address.getCity())){
                    sb.append(","+address.getCity());
                }
                if(!StrUtils.isEmpty(address.getCounty())){
                    sb.append(","+address.getCounty());
                }
                loginAdmin.setLoginLocation(sb.toString());
            }
        }
        loginAdmin.setBrowser(userAgent.getBrowser().getName());
        loginAdmin.setOs(userAgent.getOperatingSystem().getName());
    }
    
    /**
     * 从数据声明生成令牌
     *
     * @param claims 数据声明
     * @return 令牌
     */
    private String createToken(Map<String, Object> claims)
    {
        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(SIGN_KEY,SignatureAlgorithm.HS256)    // 设置安全密钥（生成签名所需的密钥和算法）
                .compact();
        return token;
    }

    /**
     * 从令牌中获取数据声明
     *
     * @param token 令牌
     * @return 数据声明
     */
    private Claims parseToken(String token)
    {
        return Jwts.parserBuilder().setSigningKey(SIGN_KEY).build().parseClaimsJws(token).getBody();
    }

    /**
     * 从令牌中获取用户名
     *
     * @param token 令牌
     * @return 用户名
     */
    public String getUsernameFromToken(String token)
    {
        Claims claims = parseToken(token);
        return claims.getSubject();
    }

    /**
     * 获取请求token
     *
     * @param request
     * @return token
     */
    private String getToken(HttpServletRequest request)
    {
        String token = request.getHeader(header);
        if (StrUtils.isNotEmpty(token) && token.startsWith(Constants.TOKEN_PREFIX))
        {
            token = token.replace(Constants.TOKEN_PREFIX, "");
        }
        return token;
    }

    private String getTokenKey(String uuid)
    {
        return Constants.LOGIN_TOKEN_KEY + uuid;
    }
}
