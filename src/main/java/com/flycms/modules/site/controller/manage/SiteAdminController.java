package com.flycms.modules.site.controller.manage;

import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.site.domain.Site;
import com.flycms.modules.site.service.ISiteService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;

/**
 * 官网设置Controller
 * 
 * @author admin
 * @date 2020-07-08
 */
@RestController
@RequestMapping("/system/site")
public class SiteAdminController extends BaseController
{
    @Autowired
    private ISiteService siteService;

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改官网设置
     */
    @PreAuthorize("@ss.hasPermi('site:site:edit')")
    @Log(title = "官网设置", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@RequestBody Site site)
    {
        return toAjax(siteService.updateSite(site));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////


    /**
     * 获取官网设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('site:site:query')")
    @GetMapping("/query")
    public AjaxResult getInfo()
    {
        return AjaxResult.success(siteService.selectSite());
    }
}
