package com.flycms.modules.site.domain;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 友情链接对象 fly_site_links
 * 
 * @author admin
 * @date 2020-07-08
 */
@Data
public class SiteLinks extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;
    /** 有钱链接类型：0文字链接，1logo链接 */
    private Integer linkType;
    /** 网站名称 */
    private String linkName;
    /** 网站地址 */
    private String linkUrl;
    /** 网站logo地址 */
    private String linkLogo;
    /** 是否显示，0不显示，1显示 */
    private Integer isShow;
    /** 排序 */
    private Integer sortOrder;
    /** 0删除，1显示 */
    private Integer deleted;
}
