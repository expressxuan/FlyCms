package com.flycms.modules.site.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 友情链接数据传输对象 fly_site_links
 * 
 * @author admin
 * @date 2020-07-08
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SiteLinksDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 有钱链接类型：0文字链接，1logo链接 */
    @Excel(name = "有钱链接类型：0文字链接，1logo链接")
    private Integer linkType;
    /** 网站名称 */
    @Excel(name = "网站名称")
    private String linkName;
    /** 网站地址 */
    @Excel(name = "网站地址")
    private String linkUrl;
    /** 网站logo地址 */
    @Excel(name = "网站logo地址")
    private String linkLogo;
    /** 是否显示，0不显示，1显示 */
    @Excel(name = "是否显示，0不显示，1显示")
    private Integer isShow;
    /** 排序 */
    @Excel(name = "排序")
    private Integer sortOrder;
}
