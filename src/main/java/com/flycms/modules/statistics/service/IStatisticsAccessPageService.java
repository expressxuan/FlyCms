package com.flycms.modules.statistics.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.statistics.domain.StatisticsAccessPage;
import com.flycms.modules.statistics.domain.dto.StatisticsAccessPageDTO;

import java.util.List;

/**
 * 受访页面日报Service接口
 * 
 * @author admin
 * @date 2021-01-12
 */
public interface IStatisticsAccessPageService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增受访页面日报
     *
     * @return 结果
     */
    public void insertStatisticsAccessPage();

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除受访页面日报
     *
     * @param ids 需要删除的受访页面日报ID
     * @return 结果
     */
    public int deleteStatisticsAccessPageByIds(Long[] ids);

    /**
     * 删除受访页面日报信息
     *
     * @param id 受访页面日报ID
     * @return 结果
     */
    public int deleteStatisticsAccessPageById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改受访页面日报
     *
     * @param statisticsAccessPage 受访页面日报
     * @return 结果
     */
    public int updateStatisticsAccessPage(StatisticsAccessPage statisticsAccessPage);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验查询受访页面是否唯一
     *
     * @param url
     * @param time
     * @return
     */
    public int checkAccessPageNameUnique(String url, int time);

    /**
     * 查询受访页面日报
     * 
     * @param id 受访页面日报ID
     * @return 受访页面日报
     */
    public StatisticsAccessPageDTO findStatisticsAccessPageById(Long id);

    /**
     * 按时间查询页面统计记录
     *
     * @param time
     * @return
     */
    public int queryStatisticsAccessPageCount(Integer time);
    /**
     * 查询受访页面日报列表
     * 
     * @param statisticsAccessPage 受访页面日报
     * @return 受访页面日报集合
     */
    public Pager<StatisticsAccessPageDTO> selectStatisticsAccessPagePager(StatisticsAccessPage statisticsAccessPage, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的受访页面日报列表
     *
     * @param statisticsAccessPage 受访页面日报
     * @return 受访页面日报集合
     */
    public List<StatisticsAccessPageDTO> exportStatisticsAccessPageList(StatisticsAccessPage statisticsAccessPage);

    /**
     * 受访页面列表
     *
     * @param time
     * @param offset
     * @param rows
     * @return
     */
    public List<StatisticsAccessPage> getAccessPageList(Integer time,Integer offset, Integer rows);
}
