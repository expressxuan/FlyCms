package com.flycms.modules.statistics.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.statistics.domain.StatisticsSource;
import com.flycms.modules.statistics.domain.dto.StatisticsSourceDTO;

import java.util.List;

/**
 * 来源统计Service接口
 * 
 * @author admin
 * @date 2020-12-09
 */
public interface IStatisticsSourceService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增来源统计
     *
     * @param statisticsSource 来源统计
     * @return 结果
     */
    public int insertStatisticsSource(StatisticsSource statisticsSource);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除来源统计
     *
     * @param ids 需要删除的来源统计ID
     * @return 结果
     */
    public int deleteStatisticsSourceByIds(Long[] ids);

    /**
     * 删除来源统计信息
     *
     * @param id 来源统计ID
     * @return 结果
     */
    public int deleteStatisticsSourceById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改来源统计
     *
     * @param statisticsSource 来源统计
     * @return 结果
     */
    public int updateStatisticsSource(StatisticsSource statisticsSource);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询来源统计
     * 
     * @param id 来源统计ID
     * @return 来源统计
     */
    public StatisticsSourceDTO findStatisticsSourceById(Long id);

    /**
     * 查询来源统计列表
     * 
     * @param statisticsSource 来源统计
     * @return 来源统计集合
     */
    public Pager<StatisticsSourceDTO> selectStatisticsSourcePager(StatisticsSource statisticsSource, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的来源统计列表
     *
     * @param statisticsSource 来源统计
     * @return 来源统计集合
     */
    public List<StatisticsSourceDTO> exportStatisticsSourceList(StatisticsSource statisticsSource);
}
