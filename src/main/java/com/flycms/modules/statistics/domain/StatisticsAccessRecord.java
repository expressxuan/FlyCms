package com.flycms.modules.statistics.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 访问记录对象 fly_statistics_access_record
 * 
 * @author admin
 * @date 2020-12-09
 */
@Data
public class StatisticsAccessRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;
    /** 是否登录访问（0-否  1-是） */
    private Boolean isLogin;
    /** 登录用户id */
    private Long loginUserId;
    /** 登录用户名 */
    private String loginUserName;
    /** 会话标识 */
    private String sessionId;
    /** cookie标识 */
    private String cookieId;
    /** 访问来源(1:PC  2:移动端H5  3:微信客户端H5 4:IOS 5:安卓 6:小程序) */
    private Short accessSourceClient;
    /** 访问网址 */
    private String accessUrl;
    /** 来源网址 */
    private String sourceUrl;
    /** 来源域名 */
    private String sourceDomain;
    /** 来源网站类型 （1-搜索引擎  2-外部链接  3-直接访问） */
    private Integer sorceUrlType;
    /** 访客ip */
    private String accessIp;
    /** 访客设备系统（如：Win10 Mac10  Android8） */
    private String accessDevice;
    /** 访客浏览器类型 */
    private String accessBrowser;
    /** 访客所属城市 */
    private String accessCity;
    /** 访客所属国家 */
    private String accessCountry;
    /** 访客所属省份 */
    private String accessProvince;
    /** 搜索名称 */
    private String engineName;
    /** 是否新访客（0:否   1:是） */
    private Integer isNewVisitor;
    /** 设备类型 */
    private Short deviceType;
    /** 搜索时间类型，1今天，2昨天，3本周，4本月，5上个月，6本年 */
    private String time;
    /** 每条url访问数量 */
    private Integer counts;
    /** 删除标识 */
    private Integer deleted;
}
