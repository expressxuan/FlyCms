package com.flycms.modules.monitor.service.impl;

import java.util.List;

import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.monitor.domain.FlyJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.monitor.domain.FlyJobLog;
import com.flycms.modules.monitor.mapper.FlyJobLogMapper;
import com.flycms.modules.monitor.service.IFlyJobLogService;

/**
 * 定时任务调度日志信息 服务层
 * 
 * @author kaifei sun
 */
@Service
public class FlyJobLogServiceImpl implements IFlyJobLogService
{
    @Autowired
    private FlyJobLogMapper jobLogMapper;
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增任务日志
     *
     * @param jobLog 调度日志信息
     */
    @Override
    public void addJobLog(FlyJobLog jobLog)
    {
        jobLogMapper.insertJobLog(jobLog);
    }
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除调度日志信息
     *
     * @param logIds 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJobLogByIds(Long[] logIds)
    {
        return jobLogMapper.deleteJobLogByIds(logIds);
    }

    /**
     * 删除任务日志
     *
     * @param jobId 调度日志ID
     */
    @Override
    public int deleteJobLogById(Long jobId)
    {
        return jobLogMapper.deleteJobLogById(jobId);
    }

    /**
     * 清空任务日志
     */
    @Override
    public void cleanJobLog()
    {
        jobLogMapper.cleanJobLog();
    }
    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////

    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 通过调度任务日志ID查询调度信息
     *
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    @Override
    public FlyJobLog selectJobLogById(Long jobLogId)
    {
        return jobLogMapper.selectJobLogById(jobLogId);
    }

    /**
     * 获取quartz调度器日志的计划任务翻页
     *
     * @param jobLog 调度日志信息
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @Override
    public Pager<FlyJobLog> selectJobLogPager(FlyJobLog jobLog, Integer page, Integer limit, String sort, String order)
    {
        Pager<FlyJobLog> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(jobLog);
        pager.setList(jobLogMapper.selectJobLogPager(pager));
        pager.setTotal(jobLogMapper.queryJobLogTotal(pager));
        return pager;
    }

    /**
     * 查询所有调度任务日志
     *
     * @return 调度任务日志列表
     */
    public List<FlyJobLog> exportJobLogList(FlyJobLog jobLog){
        return jobLogMapper.exportJobLogList(jobLog);
    }


}
