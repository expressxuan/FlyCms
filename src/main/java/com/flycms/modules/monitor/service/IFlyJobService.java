package com.flycms.modules.monitor.service;

import java.util.List;

import com.flycms.common.exception.job.TaskException;
import com.flycms.common.utils.page.Pager;
import org.quartz.SchedulerException;
import com.flycms.modules.monitor.domain.FlyJob;

/**
 * 定时任务调度信息信息 服务层
 * 
 * @author kaifei sun
 */
public interface IFlyJobService
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增任务
     *
     * @param job 调度信息
     * @return 结果
     */
    public int insertJob(FlyJob job) throws SchedulerException, TaskException;
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除任务后，所对应的trigger也将被删除
     *
     * @param job 调度信息
     * @return 结果
     */
    public int deleteJob(FlyJob job) throws SchedulerException;

    /**
     * 批量删除调度信息
     *
     * @param jobIds 需要删除的任务ID
     * @return 结果
     */
    public void deleteJobByIds(Long[] jobIds) throws SchedulerException;

    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 更新任务
     *
     * @param job 调度信息
     * @return 结果
     */
    public int updateJob(FlyJob job) throws SchedulerException, TaskException;
    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验cron表达式是否有效
     *
     * @param cronExpression 表达式
     * @return 结果
     */
    public boolean checkCronExpressionIsValid(String cronExpression);


    /**
     * 通过调度任务ID查询调度信息
     *
     * @param jobId 调度任务ID
     * @return 调度任务对象信息
     */
    public FlyJob selectJobById(Long jobId);

    /**
     * 获取quartz调度器的计划任务列表翻页
     *
     * @param job 调度信息
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    public Pager<FlyJob> selectJobPager(FlyJob job, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出调度任务
     *
     * @param job 调度任务信息
     * @return 调度任务列表
     */
    public List<FlyJob> exportJobList(FlyJob job);

    /**
     * 暂停任务
     * 
     * @param job 调度信息
     * @return 结果
     */
    public int pauseJob(FlyJob job) throws SchedulerException;

    /**
     * 恢复任务
     * 
     * @param job 调度信息
     * @return 结果
     */
    public int resumeJob(FlyJob job) throws SchedulerException;

    /**
     * 任务调度状态修改
     * 
     * @param job 调度信息
     * @return 结果
     */
    public int changeStatus(FlyJob job) throws SchedulerException;

    /**
     * 立即运行任务
     * 
     * @param job 调度信息
     * @return 结果
     */
    public void run(FlyJob job) throws SchedulerException;

}