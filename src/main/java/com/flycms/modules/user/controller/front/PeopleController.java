package com.flycms.modules.user.controller.front;

import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.domain.UserFans;
import com.flycms.modules.user.service.IUserAccountService;
import com.flycms.modules.user.service.IUserFansService;
import com.flycms.modules.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户展示Controller
 *
 * @author kaifei sun
 * @date 2020-07-08
 */
@Controller
public class PeopleController extends BaseController
{
    @Resource
    private IUserService userService;

    @Autowired
    private IUserFansService userFansService;

    @Autowired
    private IUserAccountService userAccountService;

    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    //登录处理
    @ResponseBody
    @PostMapping(value = "/people/user/follow")
    public AjaxResult userLogin(@RequestParam(value = "id", required = false) String id){
        if (!StrUtils.checkLong(id)) {
            return AjaxResult.error(0,"id参数不正确");
        }
        User people = userService.findUserById(Long.valueOf(id));
        if(people == null){
            return AjaxResult.error(1,"您关注的用户不存在");
        }
        User login = SessionUtils.getUser();
        if(login == null){
            return AjaxResult.error(2,"请登录后在关注操作");
        }
        if(userFansService.checkUserFansUnique(Long.valueOf(id),login.getId())){
            userFansService.deleteUserFansById(Long.valueOf(id),login.getId());
            userAccountService.updateUserFollow(login.getId());
            userAccountService.updateUserFans(Long.valueOf(id));
            Map<String, String> map = new HashMap<String, String>();
            map.put("count",String.valueOf(people.getCountFans()-1));
            return AjaxResult.success(3,"已取消关注",map);
        }else{
            UserFans userFans=new UserFans();
            userFans.setFollowId(Long.valueOf(id));
            userFans.setFansId(login.getId());
            userFansService.insertUserFans(userFans);
            userAccountService.updateUserFollow(login.getId());
            userAccountService.updateUserFans(Long.valueOf(id));
            System.out.println("**********"+people.getCountFollow());
            Map<String, String> map = new HashMap<String, String>();
            map.put("count",String.valueOf(people.getCountFans()+1));
            return AjaxResult.success(4,"已成功关注",map);
        }
    }

    /**
     * 用户排名首页
     *
     * @return
     */
    @GetMapping(value = {"/people-rank" , "/people-rank-p-{p}"})
    public String regUser( @PathVariable(value = "p", required = false) String p,ModelMap modelMap){
        if(p == null) p = "1";
        modelMap.addAttribute("p", p);
        return theme.getPcTemplate("people/list_people");
    }


    /**
     * 用户首页
     *
     * @return
     */
    @GetMapping(value = {"/people/{userId}", "/u/{shortUrl}"})
    public String peoleIndex(@PathVariable(value = "userId", required = false) String userId,@PathVariable(value = "shortUrl", required = false) String shortUrl, ModelMap modelMap){
        if(StrUtils.isEmpty(userId) && StrUtils.isEmpty(shortUrl)){
            return forward("error/404");
        }
        User people = null;
        //有id的时候查ID，没有的查短网址
        if(!StrUtils.isEmpty(userId)){
            if (!StrUtils.checkLong(userId)) {
                return forward("error/404");
            }
            people = userService.findUserById(Long.valueOf(userId));
            if(people == null){
                return forward("error/404");
            }
        }else if(!StrUtils.isEmpty(shortUrl)){
            people = userService.findUserByShorturl(shortUrl);
            if(people == null){
                return forward("error/404");
            }
        }
        User login = SessionUtils.getUser();
        if(login!=null){
            //当前用户不是登录用户判断审核状态，如果当前用户和登录用户id一样则可以访问自己首页
            if (!people.getId().equals(login.getId())){
                if(people.getStatus() == 0){
                    modelMap.addAttribute("message","用户未审核或者被锁定，请联系管理员！");
                    return theme.getPcTemplate("error/error_tip");
                }
                if(people.getDeleted() == 1){
                    modelMap.addAttribute("message","用户已被删除或者不存在，请联系管理员！");
                    return theme.getPcTemplate("error/error_tip");
                }
            }
        }else{
            if(people.getStatus() == 0){
                modelMap.addAttribute("message","用户未审核或者被锁定，请联系管理员！");
                return theme.getPcTemplate("error/error_tip");
            }
            if(people.getDeleted() == 1){
                modelMap.addAttribute("message","用户已被删除或者不存在，请联系管理员！");
                return theme.getPcTemplate("error/error_tip");
            }
        }

        modelMap.addAttribute("login",login);
        modelMap.addAttribute("people",people);
        return theme.getPcTemplate("people/index");
    }

    /**
     * 用户话题列表
     *
     * @return
     */
    @GetMapping(value = "/people/{userId}/topic/")
    public String regPhone(@PathVariable("userId") String userId, @PathVariable(value = "p", required = false) String p, ModelMap modelMap){
        if (!StrUtils.checkLong(userId)) {
            return forward("error/404");
        }
        User people = userService.findUserById(Long.valueOf(userId));
        if(people == null){
            return forward("error/404");
        }
        if(people.getStatus() == 0){
            modelMap.addAttribute("message","用户未审核或者被锁定，请联系管理员！");
            return theme.getPcTemplate("error/error_tip");
        }
        if(people.getDeleted() == 1){
            modelMap.addAttribute("message","用户已被删除或者不存在，请联系管理员！");
            return theme.getPcTemplate("error/error_tip");
        }
        User login = SessionUtils.getUser();
        if(p == null) p = "1";
        modelMap.addAttribute("p", p);
        modelMap.addAttribute("login",login);
        modelMap.addAttribute("people",people);
        return theme.getPcTemplate("people/list_topic");
    }

}
