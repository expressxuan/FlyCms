package com.flycms.modules.user.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.modules.user.domain.UserApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.user.mapper.UserFansMapper;
import com.flycms.modules.user.domain.UserFans;
import com.flycms.modules.user.domain.dto.UserFansDTO;
import com.flycms.modules.user.service.IUserFansService;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户分析查询关联表Service业务层处理
 * 
 * @author admin
 * @date 2020-11-27
 */
@Service
public class UserFansServiceImpl implements IUserFansService 
{
    @Autowired
    private UserFansMapper userFansMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户分析查询关联表
     *
     * @param userFans 用户分析查询关联表
     * @return 结果
     */
    @Override
    public int insertUserFans(UserFans userFans)
    {
        userFans.setId(SnowFlakeUtils.nextId());
        userFans.setCreateTime(DateUtils.getNowDate());
        return userFansMapper.insertUserFans(userFans);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户分析查询关联表信息
     *
     * @param followId 被关注者id
     * @param fansId 粉丝ID
     * @return 结果
     */
    @Override
    public int deleteUserFansById(Long followId,Long fansId)
    {
        return userFansMapper.deleteUserFansById(followId,fansId);
    }

    /**
     * 批量删除用户分析查询关联表
     *
     * @param ids 需要删除的用户分析查询关联表ID
     * @return 结果
     */
    @Override
    public int deleteUserFansByIds(Long[] ids)
    {
        return userFansMapper.deleteUserFansByIds(ids);
    }

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户分析查询关联表
     *
     * @param userFans 用户分析查询关联表
     * @return 结果
     */
    @Override
    public int updateUserFans(UserFans userFans)
    {
        return userFansMapper.updateUserFans(userFans);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询用户分析查询关联表
     *
     * @param followId 被关注者id
     * @param fansId 粉丝ID
     * @return 用户分析查询关联表
     */
    @Override
    public Boolean checkUserFansUnique(Long followId,Long fansId)
    {
        int count = userFansMapper.checkUserFansUnique(followId,fansId);
        if (count > 0)
        {
            return true;
        }
        return false;
    }

    /**
     * 查询用户分析查询关联表
     * 
     * @param id 用户分析查询关联表ID
     * @return 用户分析查询关联表
     */
    @Override
    public UserFansDTO findUserFansById(Long id)
    {
        UserFans  userFans=userFansMapper.findUserFansById(id);
        return BeanConvertor.convertBean(userFans,UserFansDTO.class);
    }


    /**
     * 查询用户分析查询关联表列表
     *
     * @param userFans 用户分析查询关联表
     * @return 用户分析查询关联表
     */
    @Override
    public Pager<UserFansDTO> selectUserFansPager(UserFans userFans, Integer page, Integer limit, String sort, String order)
    {
        Pager<UserFansDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(userFans);

        List<UserFans> userFansList=userFansMapper.selectUserFansPager(pager);
        List<UserFansDTO> dtolsit = new ArrayList<UserFansDTO>();
        userFansList.forEach(entity -> {
            UserFansDTO dto = new UserFansDTO();
            dto.setId(entity.getId());
            dto.setFollowId(entity.getFollowId());
            dto.setFansId(entity.getFansId());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(userFansMapper.queryUserFansTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的用户分析查询关联表列表
     *
     * @param userFans 用户分析查询关联表
     * @return 用户分析查询关联表集合
     */
    @Override
    public List<UserFansDTO> exportUserFansList(UserFans userFans) {
        return BeanConvertor.copyList(userFansMapper.exportUserFansList(userFans),UserFansDTO.class);
    }
}
