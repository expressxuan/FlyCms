package com.flycms.modules.user.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.WxMaConfig;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.flycms.modules.applet.domain.AppConfig;
import com.flycms.modules.applet.service.IAppConfigService;
import com.flycms.modules.user.Interceptor.LoginHandle;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WxConfig implements WebMvcConfigurer {
	@Autowired
	private WxProperties properties;
	@Autowired
	private IAppConfigService appConfigService;

	@Autowired
	public LoginHandle loginHandle;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 多个拦截器组成一个拦截器链
		// addPathPatterns 用于添加拦截规则
		// excludePathPatterns 用户排除拦截
		registry.addInterceptor(loginHandle)
				.addPathPatterns("/wx/**") //拦截所有请求
				.excludePathPatterns("/","/admin_login","/login");    //对应的不拦截的请求
	}

	@Bean
	public WxMaConfig wxMaConfig() {
		AppConfig appConfig=appConfigService.selectAppConfigById(462987116553437184l);
		WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
		config.setAppid(appConfig.getAppId());
		config.setSecret(appConfig.getAppSecret());
		return config;
	}

	@Bean
	public WxMaService wxMaService(WxMaConfig maConfig) {
		WxMaService service = new WxMaServiceImpl();
		service.setWxMaConfig(maConfig);
		return service;
	}

	@Bean
	public WxPayConfig wxPayConfig() {
		WxPayConfig payConfig = new WxPayConfig();
		payConfig.setAppId(properties.getAppId());
		payConfig.setMchId(properties.getMchId());
		payConfig.setMchKey(properties.getMchKey());
		payConfig.setNotifyUrl(properties.getNotifyUrl());
		payConfig.setKeyPath(properties.getKeyPath());
		payConfig.setTradeType("JSAPI");
		payConfig.setSignType("MD5");
		return payConfig;
	}

	@Bean
	public WxPayService wxPayService(WxPayConfig payConfig) {
		WxPayService wxPayService = new WxPayServiceImpl();
		wxPayService.setConfig(payConfig);
		return wxPayService;
	}
}