package com.flycms.modules.user.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 用户分析查询关联表对象 fly_user_fans
 * 
 * @author admin
 * @date 2020-11-27
 */
@Data
public class UserFans extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 被关注者id */
    private Long followId;
    /** 关注者id */
    private Long fansId;
}
