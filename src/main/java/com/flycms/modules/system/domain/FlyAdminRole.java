package com.flycms.modules.system.domain;

import lombok.Data;

/**
 * 用户和角色关联 fly_admin_role
 * 
 * @author kaifei sun
 */
@Data
public class FlyAdminRole
{
    /** 用户ID */
    private Long adminId;
    
    /** 角色ID */
    private Long roleId;
}
