package com.flycms.modules.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.system.domain.FlyDictType;
import com.flycms.modules.system.mapper.FlyDictDataMapper;
import com.flycms.modules.system.mapper.FlyDictTypeMapper;
import com.flycms.modules.system.domain.dto.DictTypeQueryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.flycms.modules.system.service.IFlyDictTypeService;

/**
 * 字典 业务层处理
 * 
 * @author kaifei sun
 */
@Service
public class FlyDictTypeServiceImpl implements IFlyDictTypeService
{
    @Autowired
    private FlyDictTypeMapper dictTypeMapper;

    @Autowired
    private FlyDictDataMapper dictDataMapper;

    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    @Override
    public int insertDictType(FlyDictType dictType)
    {
        return dictTypeMapper.insertDictType(dictType);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 通过字典ID删除字典信息
     * 
     * @param dictId 字典ID
     * @return 结果
     */
    @Override
    public int deleteDictTypeById(Long dictId)
    {
        return dictTypeMapper.deleteDictTypeById(dictId);
    }

    /**
     * 批量删除字典类型信息
     * 
     * @param dictIds 需要删除的字典ID
     * @return 结果
     */
    public int deleteDictTypeByIds(Long[] dictIds)
    {
        return dictTypeMapper.deleteDictTypeByIds(dictIds);
    }

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////

    /**
     * 修改保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateDictType(FlyDictType dictType)
    {
        FlyDictType oldDict = dictTypeMapper.selectDictTypeById(dictType.getDictId());
        dictDataMapper.updateDictDataType(oldDict.getDictType(), dictType.getDictType());
        return dictTypeMapper.updateDictType(dictType);
    }

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 根据字典类型ID查询信息
     *
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    @Override
    public FlyDictType selectDictTypeById(Long dictId)
    {
        return dictTypeMapper.selectDictTypeById(dictId);
    }

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    public FlyDictType selectDictTypeByType(String dictType)
    {
        return dictTypeMapper.selectDictTypeByType(dictType);
    }


    /**
     * 校验字典类型称是否唯一
     * 
     * @param dict 字典类型
     * @return 结果
     */
    @Override
    public String checkDictTypeUnique(FlyDictType dict)
    {
        Long dictId = StrUtils.isNull(dict.getDictId()) ? -1L : dict.getDictId();
        FlyDictType dictType = dictTypeMapper.checkDictTypeUnique(dict.getDictType());
        if (StrUtils.isNotNull(dictType) && dictType.getDictId().longValue() != dictId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    @Override
    public List<FlyDictType> selectDictTypeList(FlyDictType dictType)
    {
        return dictTypeMapper.selectDictTypeList(dictType);
    }

    /**
     * 根据所有字典类型
     *
     * @return 字典类型集合信息
     */
    @Override
    public List<FlyDictType> selectDictTypeAll()
    {
        return dictTypeMapper.selectDictTypeAll();
    }

    /**
     * 根据条件分页查询字典类型
     *
     * @param flyDictType 字典类型
     * @return 字典类型
     */
    @Override
    public Pager<DictTypeQueryDTO> selectDictTypePager(FlyDictType flyDictType, Integer page, Integer limit, String sort, String order)
    {
        Pager<DictTypeQueryDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(flyDictType);

        List<FlyDictType> flyDictTypeList=dictTypeMapper.selectDictTypePager(pager);
        List<DictTypeQueryDTO> dtolsit = new ArrayList<DictTypeQueryDTO>();
        flyDictTypeList.forEach(entity -> {
            DictTypeQueryDTO dto = new DictTypeQueryDTO();
            dto.setDictId(entity.getDictId());
            dto.setDictName(entity.getDictName());
            dto.setDictType(entity.getDictType());
            dto.setRemark(entity.getRemark());
            dto.setStatus(entity.getStatus());
            dto.setCreateTime(entity.getCreateTime());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(dictTypeMapper.queryDictTypeTotal(pager));
        return pager;
    }
}
