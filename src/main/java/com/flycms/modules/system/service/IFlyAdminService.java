package com.flycms.modules.system.service;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.system.domain.FlyAdmin;
import com.flycms.modules.system.domain.dto.AdminQueryDTO;

/**
 * 用户 业务层
 * 
 * @author kaifei sun
 */
public interface IFlyAdminService
{
    /**
     * 根据条件分页查询用户列表
     * 
     * @param admin 用户信息
     * @return 用户信息集合信息
     */
    public Pager<FlyAdmin> selectAdminPager(FlyAdmin admin, Integer page, Integer limit, String sort, String order);

    /**
     * 通过用户名查询用户
     * 
     * @param adminName 用户名
     * @return 用户对象信息
     */
    public FlyAdmin selectAdminByAdminName(String adminName);

    /**
     * 通过用户ID查询用户
     *
     * @param adminId 用户ID
     * @return 用户对象信息
     */
    public AdminQueryDTO findAdminById(Long adminId);

    /**
     * 根据用户ID查询用户所属角色组
     * 
     * @param adminName 用户名
     * @return 结果
     */
    public String selectAdminRoleGroup(String adminName);

    /**
     * 根据用户ID查询用户所属岗位组
     * 
     * @param adminName 用户名
     * @return 结果
     */
    public String selectAdminPostGroup(String adminName);

    /**
     * 校验用户名称是否唯一
     * 
     * @param adminName 用户名称
     * @return 结果
     */
    public String checkAdminNameUnique(String adminName);

    /**
     * 校验手机号码是否唯一
     *
     * @param admin 用户信息
     * @return 结果
     */
    public String checkPhoneUnique(FlyAdmin admin);

    /**
     * 校验email是否唯一
     *
     * @param admin 用户信息
     * @return 结果
     */
    public String checkEmailUnique(FlyAdmin admin);

    /**
     * 校验用户是否允许操作
     * 
     * @param admin 用户信息
     */
    public void checkAdminAllowed(FlyAdmin admin);

    /**
     * 新增用户信息
     * 
     * @param admin 用户信息
     * @return 结果
     */
    public int insertAdmin(FlyAdmin admin);

    /**
     * 修改用户信息
     * 
     * @param admin 用户信息
     * @return 结果
     */
    public int updateAdmin(FlyAdmin admin);

    /**
     * 修改用户状态
     * 
     * @param admin 用户信息
     * @return 结果
     */
    public int updateAdminStatus(FlyAdmin admin);

    /**
     * 修改用户基本信息
     * 
     * @param admin 用户信息
     * @return 结果
     */
    public int updateAdminProfile(FlyAdmin admin);

    /**
     * 修改用户头像
     * 
     * @param adminName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public boolean updateAdminAvatar(String adminName, String avatar);

    /**
     * 重置用户密码
     * 
     * @param admin 用户信息
     * @return 结果
     */
    public int resetPwd(FlyAdmin admin);

    /**
     * 重置用户密码
     * 
     * @param adminName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetAdminPwd(String adminName, String password);

    /**
     * 通过用户ID删除用户
     * 
     * @param adminId 用户ID
     * @return 结果
     */
    public int deleteAdminById(Long adminId);

    /**
     * 批量删除用户信息
     * 
     * @param adminIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteAdminByIds(Long[] adminIds);

    /**
     * 导入用户数据
     * 
     * @param adminList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importAdmin(List<FlyAdmin> adminList, Boolean isUpdateSupport, String operName);
}
