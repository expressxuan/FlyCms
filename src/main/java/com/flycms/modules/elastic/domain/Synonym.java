package com.flycms.modules.elastic.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 同义词词库对象 fly_synonym
 * 
 * @author admin
 * @date 2020-10-22
 */
@Data
public class Synonym extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 同义词 */
    private String synonymWord;
}
