package com.flycms.modules.elastic.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.elastic.domain.Synonym;
import com.flycms.modules.elastic.domain.dto.SynonymDTO;

import java.util.List;

/**
 * 同义词词库Service接口
 * 
 * @author admin
 * @date 2020-10-22
 */
public interface ISynonymService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增同义词词库
     *
     * @param synonym 同义词词库
     * @return 结果
     */
    public int insertSynonym(Synonym synonym);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除同义词词库
     *
     * @param ids 需要删除的同义词词库ID
     * @return 结果
     */
    public int deleteSynonymByIds(Long[] ids);

    /**
     * 删除同义词词库信息
     *
     * @param id 同义词词库ID
     * @return 结果
     */
    public int deleteSynonymById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改同义词词库
     *
     * @param synonym 同义词词库
     * @return 结果
     */
    public int updateSynonym(Synonym synonym);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验同义词是否唯一
     *
     * @param synonym 同义词词库
     * @return 结果
     */
    public String checkSynonymSynonymWordUnique(Synonym synonym);


    /**
     * 查询同义词词库
     * 
     * @param id 同义词词库ID
     * @return 同义词词库
     */
    public SynonymDTO findSynonymById(Long id);

    /**
     * 查询同义词词库列表
     * 
     * @param synonym 同义词词库
     * @return 同义词词库集合
     */
    public Pager<SynonymDTO> selectSynonymPager(Synonym synonym, Integer page, Integer limit, String sort, String order);

    /**
     * 查询所有同义词词库列表
     *
     * @return
     */
    public List<Synonym> selectSynonymList();
}
