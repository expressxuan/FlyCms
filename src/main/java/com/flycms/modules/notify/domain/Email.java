package com.flycms.modules.notify.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 邮件服务器对象 fly_email
 * 
 * @author admin
 * @date 2020-11-09
 */
@Data
public class Email extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 发件箱KEY */
    private String mailServer;
    /** 发件服务器 */
    private String mailSmtpServer;
    /** 发件人昵称 */
    private String mailUserName;
    /** 发件箱账号 */
    private String mailSmtpUsermail;
    /** 发件箱密码 */
    private String mailSmtpPassword;
    /** 发件箱端口 */
    private String mailSmtpPort;
}
