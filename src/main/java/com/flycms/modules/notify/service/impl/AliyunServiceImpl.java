package com.flycms.modules.notify.service.impl;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.flycms.common.utils.CharUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.modules.notify.domain.SmsCode;
import com.flycms.modules.notify.domain.po.SmsApiUnionSignPO;
import com.flycms.modules.notify.service.IAliyunService;
import com.flycms.modules.notify.service.ISmsApiService;
import com.flycms.modules.notify.service.ISmsCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 9:58 2019/9/3
 */
@Service
public class AliyunServiceImpl implements IAliyunService {
    @Autowired
    private ISmsApiService smsApiService;
    @Autowired
    private ISmsCodeService smsCodeService;

    /* 短信API产品名称（短信产品名固定，无需修改） */
    private static final String product = "Dysmsapi";

    /* 短信API产品域名，接口地址固定，无需修改 */
    private static final String domain = "dysmsapi.aliyuncs.com";

    /* 此处需要替换成开发者自己的accessKeyId和accessKeySecret(在阿里云访问控制台寻找) */
    //private static final String accessKeyId = "LTAIrDwtR6J1kvdW"; //TODO: 这里要写成你自己生成的
    //private static final String accessKeySecret = "EMCuJhkLkq7jfPavu1ZdUWd6F8fVkr";//TODO: 这里要写成你自己生成的

    /* 短信发送 */
    @Override
    public Boolean getSendSms(SmsCode smsCode) {
        SmsApiUnionSignPO api = smsApiService.findSmsApiUnionSignByTpId(smsCode.getTemplateId());
        /* 超时时间，可自主调整 */
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        try {
            /* 初始化acsClient,暂不支持region化 */
            //IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", api.getAccessKeyId(), api.getAccessKeySecret());
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);

            /* 组装请求对象-具体描述见控制台-文档部分内容 */
            SendSmsRequest request = new SendSmsRequest();
            /* 必填:待发送手机号 */
            request.setPhoneNumbers(smsCode.getUserName());
            /* 必填:短信签名-可在短信控制台中找到 */
            request.setSignName(api.getSignName()); //TODO: 这里是你短信签名
            /* 必填:短信模板code-可在短信控制台中找到 */
            request.setTemplateCode(api.getTemplateCode()); //TODO: 这里是你的模板code
            /* 可选:模板中的变量替换JSON串,如模板内容为"亲爱的用户,您的验证码为${code}"时,此处的值为 */
            request.setTemplateParam("{\"code\":\"" + smsCode.getCode() + "\"}");

            // hint 此处可能会抛出异常，注意catch
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            if(sendSmsResponse.getCode()!= null && sendSmsResponse.getCode().equals("OK")){
                smsCode.setId(SnowFlakeUtils.nextId());
                smsCode.setReferStatus(0);
                smsCode.setCreateTime(new Date());
                smsCodeService.insertSmsCode(smsCode);
                return true;
            }

        }  catch (ClientException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
