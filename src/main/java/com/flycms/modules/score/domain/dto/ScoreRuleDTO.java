package com.flycms.modules.score.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 积分规则数据传输对象 fly_score_rule
 *
 * @author admin
 * @date 2020-12-01
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class ScoreRuleDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 规则名称 */
    @Excel(name = "规则名称")
    private String name;
    /** 规则说明 */
    @Excel(name = "规则说明")
    private String remark;
    /** 变化积分 */
    @Excel(name = "变化积分")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer score;
    /** 奖励次数类型，day每天一次，week每周一次，month每月一次，year每年一次，one只有一次，unlimite不限次数 */
    @Excel(name = "奖励周期")
    private String type;
    /** 状态，0禁用，1启用 */
    @Excel(name = "状态，0禁用，1启用")
    private String status;
    /** 创建时间 */
    @Excel(name = "添加时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}