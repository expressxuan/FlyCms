package com.flycms.modules.score.service.impl;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.score.domain.ScoreDetail;
import com.flycms.modules.score.domain.dto.ScoreDetailDTO;
import com.flycms.modules.score.mapper.ScoreDetailMapper;
import com.flycms.modules.score.service.IScoreDetailService;
import com.flycms.modules.user.service.IUserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * 开发公司：97560.com<br/>
 * 版权：97560.com<br/>
 * <p>
 * 
 * 积分模块---积分记录服务类
 * 
 * <p>
 * 
 * 区分　责任人　日期　　　　说明<br/>
 * 创建　孙开飞　2017年5月25日 　<br/>
 * <p>
 * *******
 * <p>
 * 
 * @author sun-kaifei
 * @email admin@97560.com
 * @version 1.0,2017年7月25日 <br/>
 * 
 */
@Service
public class ScoreDetailServiceImpl implements IScoreDetailService {
    @Resource
    private ScoreDetailMapper scoreDetailMapper;
    @Autowired
    private IUserAccountService userAccountService;
    // ///////////////////////////////
    // /////     增加         ////////
    // ///////////////////////////////
    /**
     * 保存用户积分记录
     *
     * @param scoreDetail
     *        积分记录实体
     * @param calculate
     *        运算：plus是加+,reduce是减-
     * @return
     */
    public int saveScoreDetail(ScoreDetail scoreDetail, String calculate) {
        //更新用户积分
        userAccountService.updateUserAccountScore(calculate,scoreDetail.getScore(), scoreDetail.getUserId());
        scoreDetail.setId(SnowFlakeUtils.nextId());
        scoreDetail.setCreateTime(new Date());
        return scoreDetailMapper.saveScoreDetail(scoreDetail);
    }

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 修改奖励记录
     *
     * @param id
     *
     */
    public void scoreDetailByCancel(Long id) {
        scoreDetailMapper.scoreDetailByCancel(id);
    }


    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////

    /**
     * 批量删除积分日志
     *
     * @param ids 需要删除的积分日志ID
     * @return 结果
     */
    @Override
    public int deleteScoreDetailByIds(Long[] ids)
    {
        return scoreDetailMapper.deleteScoreDetailByIds(ids);
    }

    /**
     * 删除积分日志信息
     *
     * @param id 积分日志ID
     * @return 结果
     */
    @Override
    public int deleteScoreDetailById(Long id)
    {
        return scoreDetailMapper.deleteScoreDetailById(id);
    }

    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////
    /**
     * 查询积分日志
     *
     * @param id 积分日志ID
     * @return 积分日志
     */
    @Override
    public ScoreDetailDTO findScoreDetailById(Long id)
    {
        ScoreDetail  scoreDetail=scoreDetailMapper.findScoreDetailById(id);
        return BeanConvertor.convertBean(scoreDetail,ScoreDetailDTO.class);
    }

    /**
     * 是否能奖励，true表示可以奖励
     * @param userId
     * @param scoreRuleId
     * @param type
     * @return
     */
    public boolean scoreDetailCanBonus(Long userId, Long scoreRuleId, String type) {
        List<ScoreDetail> list = scoreDetailMapper.scoreDetailCanBonus(userId,scoreRuleId,type);
        return list.size() == 0;
    }

    /**
     * 根据会员、获取奖励的外键、奖励规则ID获取奖励激励，不包括foreign_id=0
     * @param userId
     * @param scoreRuleId
     * @param forgignId
     * @return
     */
    public ScoreDetail findByForeignAndRule(Long userId, Long scoreRuleId, Long forgignId) {
        return scoreDetailMapper.findByForeignAndRule(userId,scoreRuleId,forgignId);
    }

    /**
     * 查询积分日志列表
     *
     * @param scoreDetail 积分日志
     * @return 积分日志
     */
    @Override
    public Pager<ScoreDetailDTO> selectScoreDetailPager(ScoreDetail scoreDetail, Integer page, Integer limit, String sort, String order)
    {
        Pager<ScoreDetailDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(scoreDetail);

        List<ScoreDetail> scoreDetailList=scoreDetailMapper.selectScoreDetailPager(pager);
        List<ScoreDetailDTO> dtolsit = new ArrayList<ScoreDetailDTO>();
        scoreDetailList.forEach(entity -> {
            ScoreDetailDTO dto = new ScoreDetailDTO();
            dto.setId(entity.getId());
            dto.setType(entity.getType());
            dto.setUserId(entity.getUserId());
            dto.setScore(entity.getScore());
            dto.setBalance(entity.getBalance());
            dto.setForeignId(entity.getForeignId());
            dto.setScoreRuleId(entity.getScoreRuleId());
            dto.setStatus(entity.getStatus());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(scoreDetailMapper.queryScoreDetailTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的积分日志列表
     *
     * @param scoreDetail 积分日志
     * @return 积分日志集合
     */
    @Override
    public List<ScoreDetailDTO> exportScoreDetailList(ScoreDetail scoreDetail) {
        return BeanConvertor.copyList(scoreDetailMapper.exportScoreDetailList(scoreDetail),ScoreDetailDTO.class);
    }
}
