package com.flycms.modules.fullname.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.fullname.domain.SearchKeyword;
import org.springframework.stereotype.Repository;

/**
 * 姓名搜索Mapper接口
 * 
 * @author admin
 * @date 2020-10-13
 */
@Repository
public interface SearchKeywordMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增姓名搜索
     *
     * @param searchKeyword 姓名搜索
     * @return 结果
     */
    public int insertSearchKeyword(SearchKeyword searchKeyword);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除姓名搜索
     *
     * @param id 姓名搜索ID
     * @return 结果
     */
    public int deleteSearchKeywordById(Long id);

    /**
     * 批量删除姓名搜索
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSearchKeywordByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改姓名搜索
     *
     * @param searchKeyword 姓名搜索
     * @return 结果
     */
    public int updateSearchKeyword(SearchKeyword searchKeyword);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验姓名是否唯一
     *
     * @param searchKeyword 姓名搜索ID
     * @return 结果
     */
    public int checkSearchKeywordFullNameUnique(SearchKeyword  searchKeyword);


    /**
     * 查询姓名搜索
     * 
     * @param id 姓名搜索ID
     * @return 姓名搜索
     */
    public SearchKeyword findSearchKeywordById(Long id);

    /**
     * 查询姓名搜索数量
     *
     * @param pager 分页处理类
     * @return 姓名搜索数量
     */
    public int querySearchKeywordTotal(Pager pager);

    /**
     * 查询姓名搜索列表
     * 
     * @param pager 分页处理类
     * @return 姓名搜索集合
     */
    public List<SearchKeyword> selectSearchKeywordPager(Pager pager);

}
