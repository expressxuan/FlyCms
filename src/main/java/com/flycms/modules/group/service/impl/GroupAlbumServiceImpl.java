package com.flycms.modules.group.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.group.mapper.GroupAlbumMapper;
import com.flycms.modules.group.domain.GroupAlbum;
import com.flycms.modules.group.domain.dto.GroupAlbumDTO;
import com.flycms.modules.group.service.IGroupAlbumService;

import java.util.ArrayList;
import java.util.List;

/**
 * 小组帖子专辑Service业务层处理
 * 
 * @author admin
 * @date 2020-12-16
 */
@Service
public class GroupAlbumServiceImpl implements IGroupAlbumService 
{
    @Autowired
    private GroupAlbumMapper groupAlbumMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组帖子专辑
     *
     * @param groupAlbum 小组帖子专辑
     * @return 结果
     */
    @Override
    public int insertGroupAlbum(GroupAlbum groupAlbum)
    {
        groupAlbum.setId(SnowFlakeUtils.nextId());
        groupAlbum.setUserId(SessionUtils.getUser().getId());
        groupAlbum.setCreateTime(DateUtils.getNowDate());
        return groupAlbumMapper.insertGroupAlbum(groupAlbum);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组帖子专辑
     *
     * @param ids 需要删除的小组帖子专辑ID
     * @return 结果
     */
    @Override
    public int deleteGroupAlbumByIds(Long[] ids)
    {
        return groupAlbumMapper.deleteGroupAlbumByIds(ids);
    }

    /**
     * 删除小组帖子专辑信息
     *
     * @param id 小组帖子专辑ID
     * @return 结果
     */
    @Override
    public int deleteGroupAlbumById(Long id)
    {
        return groupAlbumMapper.deleteGroupAlbumById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组帖子专辑
     *
     * @param groupAlbum 小组帖子专辑
     * @return 结果
     */
    @Override
    public int updateGroupAlbum(GroupAlbum groupAlbum)
    {
        return groupAlbumMapper.updateGroupAlbum(groupAlbum);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验专辑名字是否唯一
     *
     * @param id 专辑ID
     * @param albumName 专辑名字
     * @return 结果
     */
     @Override
     public String checkGroupAlbumAlbumNameUnique(Long id,String albumName)
     {
         GroupAlbum groupAlbum = new GroupAlbum();
         groupAlbum.setId(id);
         groupAlbum.setAlbumName(albumName);
         int count = groupAlbumMapper.checkGroupAlbumAlbumNameUnique(groupAlbum);
         if (count > 0){
             return UserConstants.NOT_UNIQUE;
         }
         return UserConstants.UNIQUE;
     }


    /**
     * 查询小组帖子专辑
     * 
     * @param id 小组帖子专辑ID
     * @return 小组帖子专辑
     */
    @Override
    public GroupAlbumDTO findGroupAlbumById(Long id)
    {
        GroupAlbum groupAlbum=groupAlbumMapper.findGroupAlbumById(id);
        return BeanConvertor.convertBean(groupAlbum,GroupAlbumDTO.class);
    }


    /**
     * 查询小组帖子专辑列表
     *
     * @param groupAlbum 小组帖子专辑
     * @return 小组帖子专辑
     */
    @Override
    public Pager<GroupAlbumDTO> selectGroupAlbumPager(GroupAlbum groupAlbum, Integer page, Integer limit, String sort, String order)
    {
        Pager<GroupAlbumDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupAlbum);

        List<GroupAlbum> groupAlbumList=groupAlbumMapper.selectGroupAlbumPager(pager);
        List<GroupAlbumDTO> dtolsit = new ArrayList<GroupAlbumDTO>();
        groupAlbumList.forEach(entity -> {
            GroupAlbumDTO dto = new GroupAlbumDTO();
            dto.setId(entity.getId());
            dto.setUserId(entity.getUserId());
            dto.setGroupId(entity.getGroupId());
            dto.setAlbumPic(entity.getAlbumPic());
            dto.setAlbumName(entity.getAlbumName());
            dto.setAlbumDesc(entity.getAlbumDesc());
            dto.setCountTopic(entity.getCountTopic());
            dto.setIsaudit(entity.getIsaudit());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupAlbumMapper.queryGroupAlbumTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的小组帖子专辑列表
     *
     * @param groupAlbum 小组帖子专辑
     * @return 小组帖子专辑集合
     */
    @Override
    public List<GroupAlbumDTO> exportGroupAlbumList(GroupAlbum groupAlbum) {
        return BeanConvertor.copyList(groupAlbumMapper.exportGroupAlbumList(groupAlbum),GroupAlbumDTO.class);
    }
}
