package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.group.domain.GroupUser;
import com.flycms.modules.group.domain.dto.GroupUserDTO;

/**
 * 群组和用户对应关系Service接口
 * 
 * @author admin
 * @date 2020-09-27
 */
public interface IGroupUserService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增群组和用户对应关系
     *
     * @param groupUser 群组和用户对应关系
     * @return 结果
     */
    public AjaxResult insertGroupUser(GroupUser groupUser);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除群组和用户对应关系信息
     *
     * @param userId 群组和用户对应关系ID
     * @return 结果
     */
    public int deleteGroupUserById(Long userId,Long groupId);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改群组和用户对应关系
     *
     * @param groupUser 群组和用户对应关系
     * @return 结果
     */
    public int updateGroupUser(GroupUser groupUser);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 按用户ID和群组ID查询关系详细信息
     *
     * @param groupUser 群组和用户对应关系
     * @return 群组和用户对应关系
     */
    public GroupUser findGroupUser(GroupUser groupUser);

    /**
     * 查询群组和用户对应关系列表
     * 
     * @param groupUser 群组和用户对应关系
     * @return 群组和用户对应关系集合
     */
    public Pager<GroupUserDTO> selectGroupUserPager(GroupUser groupUser, Integer page, Integer limit, String sort, String order);
}
