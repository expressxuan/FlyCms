package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.dto.GroupDTO;

/**
 * 群组(小组)Service接口
 * 
 * @author admin
 * @date 2020-09-25
 */
public interface IGroupService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增群组(小组)
     *
     * @param group 群组(小组)
     * @return 结果
     */
    public int insertGroup(Group group);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除群组(小组)
     *
     * @param ids 需要删除的群组(小组)ID
     * @return 结果
     */
    public int deleteGroupByIds(Long[] ids);

    /**
     * 删除群组(小组)信息
     *
     * @param id 群组(小组)ID
     * @return 结果
     */
    public int deleteGroupById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改群组(小组)
     *
     * @param group 群组(小组)
     * @return 结果
     */
    public int updateGroup(Group group);

    /**
     * 更新小组内发布话题数量
     *
     * @param id 群组(小组)ID
     * @return
     */
    public int updateCountTopic(Long id);

    /**
     * 更新小组加入人数数量
     *
     * @param id 群组(小组)ID
     * @return
     */
    public int updateCountUser(Long id);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验群组名称是否唯一
     *
     * @param group 群组(小组)
     * @return 结果
     */
    public String checkGroupGroupNameUnique(Group group);

    /**
     * 查询群组(小组)短域名是否被占用
     *
     * @param shortUrl 群组(小组)
     * @return 结果
     */
    public String checkGroupShorturlUnique(String shortUrl);

    /**
     * 查询群组(小组)
     * 
     * @param id 群组(小组)ID
     * @return 群组(小组)
     */
    public Group findGroupById(Long id);

    /**
     * 按shortUrl查询群组(小组)信息
     *
     * @param shortUrl 短域名地址
     * @return 群组(小组)
     */
    public Group findGroupByShorturl(String shortUrl);

    /**
     * 查询群组(小组)列表
     * 
     * @param group 群组(小组)
     * @return 群组(小组)集合
     */
    public Pager<GroupDTO> selectGroupPager(Group group, Integer page, Integer limit, String sort, String order);
}
