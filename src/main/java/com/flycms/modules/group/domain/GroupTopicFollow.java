package com.flycms.modules.group.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 话题关注对象 fly_group_topic_follow
 * 
 * @author admin
 * @date 2021-02-01
 */
@Data
public class GroupTopicFollow extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long id;
    /** 标签ID */
    private Long topicId;
    /** 用户UID */
    private Long userId;
}
