package com.flycms.modules.group.domain;

import java.util.Date;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 小组成员审核对象 fly_group_user_isaudit
 * 
 * @author admin
 * @date 2020-11-24
 */
@Data
public class GroupUserIsaudit extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    /** ID */
    private Long id;
    /** 用户ID */
    private Long userId;
    /** 小组ID */
    private Long groupId;
    /** 审核状态 */
    private Integer status;
    /** 审核人id */
    private Long reviewer;
    /** 审核时间 */
    private Date auditTime;
}
