package com.flycms.modules.group.domain.vo;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 群组(小组)对象 fly_group
 * 
 * @author admin
 * @date 2020-09-25
 */
@Data
public class GroupVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 小组ID */
    private Long id;
    /** 用户ID */
    private Long userId;
    /** 分类ID */
    private Long columnId;
    /** 群组名称 */
    private String groupName;
    /** 小组介绍 */
    private String groupDesc;
    /** 图标路径 */
    private String path;
    /** 小组图标 */
    private String photo;
    /** 背景图片 */
    private String bgphoto;
    /** 帖子统计 */
    private Integer countTopic;
    /** 今天发帖 */
    private Integer countTopicToday;
    /** 成员数量 */
    private Integer countUser;
    /** 未审数量 */
    private Integer countTopicAudit;
    /** 加入方式 */
    private Integer joinway;
    /** 支付金额 */
    private Double price;
    /** 组长角色名称 */
    private String roleLeader;
    /** 管理员角色名称 */
    private String roleAdmin;
    /** 成员角色名称 */
    private String roleUser;
    /** 是否推荐 */
    private Integer isrecommend;
    /** 是否公开 0公开，1私密 */
    private Integer isopen;
    /** 是否审核 */
    private Integer isaudit;
    /** 允许发帖 0不允许，1允许 */
    private Integer ispost;
    /** 是否显示 */
    private Integer isshow;
    /** 是否发帖审核  0不允许，1允许 */
    private Integer ispostaudit;
    /** 排序ID */
    private Integer sortOrder;
    /** 审核状态 */
    private Integer status;
}
