package com.flycms.modules.group.domain.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 小组话题数据传输对象 fly_group_topic
 * 
 * @author admin
 * @date 2020-09-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class AdminGroupTopicDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 话题ID */
    @Excel(name = "话题ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 分类ID */
    @Excel(name = "分类ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long columnId;
    /** 分类名称 */
    @Excel(name = "分类名称")
    private String columnName;
    /** 小组ID */
    @Excel(name = "小组ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;
    /** 小组名称 */
    @Excel(name = "小组名称")
    private String groupName;
    /** 用户ID */
    @Excel(name = "用户ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 帖子标题 */
    @Excel(name = "帖子标题")
    private String title;
    /** 是否置顶 */
    @Excel(name = "是否置顶")
    private String istop;
    /** 是否关闭 */
    @Excel(name = "是否关闭")
    private String isclose;
    /** 是否允许评论 */
    @Excel(name = "是否允许评论,0允许")
    private String iscomment;
    /** 是否评论后显示内容 */
    @Excel(name = "是否评论后显示内容")
    private String iscommentshow;
    /** 是否精华帖子 */
    @Excel(name = "是否精华帖子")
    private String isposts;
    /** 审核 */
    @Excel(name = "审核")
    private String isaudit;
    /** 推荐 */
    @Excel(name = "推荐")
    private String[] recommends;
    /** 审核状态 */
    @Excel(name = "审核状态")
    private String status;
    /** 创建时间 */
    @Excel(name = "添加时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
