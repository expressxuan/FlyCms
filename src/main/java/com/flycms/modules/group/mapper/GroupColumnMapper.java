package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupColumn;
import org.springframework.stereotype.Repository;

/**
 * 小组分类Mapper接口
 * 
 * @author admin
 * @date 2020-09-25
 */
@Repository
public interface GroupColumnMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组分类
     *
     * @param groupColumn 小组分类
     * @return 结果
     */
    public int insertGroupColumn(GroupColumn groupColumn);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组分类
     *
     * @param id 小组分类ID
     * @return 结果
     */
    public int deleteGroupColumnById(Long id);

    /**
     * 批量删除小组分类
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupColumnByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组分类
     *
     * @param groupColumn 小组分类
     * @return 结果
     */
    public int updateGroupColumn(GroupColumn groupColumn);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验分类名称是否唯一
     *
     * @param groupColumn 小组分类ID
     * @return 结果
     */
    public int checkGroupColumnColumnNameUnique(GroupColumn  groupColumn);


    /**
     * 查询小组分类
     * 
     * @param id 小组分类ID
     * @return 小组分类
     */
    public GroupColumn findGroupColumnById(Long id);

    /**
     * 查询小组分类数量
     *
     * @param pager 分页处理类
     * @return 小组分类数量
     */
    public int queryGroupColumnTotal(Pager pager);

    /**
     * 查询小组分类列表
     *
     * @param groupColumn 小组分类
     * @return 小组分类集合
     */
    public List<GroupColumn> selectGroupColumnList(GroupColumn groupColumn);

}
