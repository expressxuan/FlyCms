package com.flycms.modules.data.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 内容与标签关联对象 fly_label_merge
 * 
 * @author admin
 * @date 2020-11-18
 */
@Data
public class LabelMerge extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标签id */
    private Long labelId;
    /** 信息id */
    private Long infoId;
    /** 信息类别 */
    private Integer infoType;
}
