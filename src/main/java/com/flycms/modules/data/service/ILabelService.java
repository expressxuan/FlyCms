package com.flycms.modules.data.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.domain.Label;
import com.flycms.modules.data.domain.dto.LabelDTO;

import java.util.List;

/**
 * 标签Service接口
 * 
 * @author admin
 * @date 2020-11-18
 */
public interface ILabelService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增标签
     *
     * @param label 标签
     * @return 结果
     */
    public int insertLabel(Label label);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除标签
     *
     * @param ids 需要删除的标签ID
     * @return 结果
     */
    public int deleteLabelByIds(Long[] ids);

    /**
     * 删除标签信息
     *
     * @param id 标签ID
     * @return 结果
     */
    public int deleteLabelById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改标签
     *
     * @param label 标签
     * @return 结果
     */
    public int updateLabel(Label label);

    /**
     * 按id更新标签关联话题统计信息
     *
     * @param id 标签ID
     * @param infoType 信息类别，1话题，2小组
     * @return 结果
     */
    public int updateTopicByCount(Long id,int infoType);

    /**
     * 更新标签被关注数量
     *
     * @param id 标签ID
     * @return
     */
    public int updateLabelFollowCount(Long id);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验标签名称是否唯一
     *
     * @param label 标签实体类
     * @return 结果
     */
    public String checkLabelLabelNameUnique(Label label);

    /**
     * 校验短网址是否唯一
     *
     * @param shortUrl 短网址字符串
     * @return 结果
     */
    public String checkLabelShorturlUnique(String shortUrl);

    /**
     * 查询标签短域名是否被占用
     *
     * @return
     */
    public String shortUrl();

    /**
     * 按shortUrl查询标签信息
     *
     * @param shortUrl
     * @return
     */
    public Label findLabelByShorturl(String shortUrl);
    /**
     * 查询标签
     * 
     * @param id 标签ID
     * @return 标签
     */
    public LabelDTO findLabelById(Long id);

    /**
     * title查询标签
     *
     * @param title 标签
     * @return 标签
     */
    public Label findLabelByTitle(String title);

    /**
     * 查询标签列表
     * 
     * @param label 标签
     * @return 标签集合
     */
    public Pager<LabelDTO> selectLabelPager(Label label, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的标签列表
     *
     * @param label 标签
     * @return 标签集合
     */
    public List<LabelDTO> exportLabelList(Label label);
}
