package com.flycms.modules.data.service.impl;


import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.mapper.LabelMergeMapper;
import com.flycms.modules.data.domain.LabelMerge;
import com.flycms.modules.data.domain.dto.LabelMergeDTO;
import com.flycms.modules.data.service.ILabelMergeService;

import java.util.ArrayList;
import java.util.List;

/**
 * 内容与标签关联Service业务层处理
 * 
 * @author admin
 * @date 2020-11-18
 */
@Service
public class LabelMergeServiceImpl implements ILabelMergeService 
{
    @Autowired
    private LabelMergeMapper labelMergeMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增内容与标签关联
     *
     * @param labelMerge 内容与标签关联
     * @return 结果
     */
    @Override
    public int insertLabelMerge(LabelMerge labelMerge)
    {
        return labelMergeMapper.insertLabelMerge(labelMerge);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除内容与标签关联
     *
     * @param labelIds 需要删除的内容与标签关联ID
     * @return 结果
     */
    @Override
    public int deleteLabelMergeByIds(Long[] labelIds)
    {
        return labelMergeMapper.deleteLabelMergeByIds(labelIds);
    }

    /**
     * 删除内容与标签关联信息
     *
     * @param labelId 内容与标签关联ID
     * @return 结果
     */
    @Override
    public int deleteLabelMergeById(Long labelId)
    {
        return labelMergeMapper.deleteLabelMergeById(labelId);
    }

    /**
     * 按内容id和标签id删除内容与标签关联
     *
     * @param labelId
     * @param infoId
     * @return
     */
    @Override
    public int deleteLabelMerge(Long labelId,Long infoId)
    {
        return labelMergeMapper.deleteLabelMerge(labelId,infoId);
    }

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 查询标签关联是否已关联
     *
     * @param labelMerge 关联实体类
     * @return
     */
    @Override
    public boolean checkLabelMergeUnique(LabelMerge labelMerge){
        int totalCount = labelMergeMapper.checkLabelMergeUnique(labelMerge);
        return totalCount > 0 ? true : false;
    }
    /**
     * 修改内容与标签关联
     *
     * @param labelMerge 内容与标签关联
     * @return 结果
     */
    @Override
    public int updateLabelMerge(LabelMerge labelMerge)
    {
        return labelMergeMapper.updateLabelMerge(labelMerge);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询内容与标签关联
     * 
     * @param labelId 内容与标签关联ID
     * @return 内容与标签关联
     */
    @Override
    public LabelMergeDTO findLabelMergeById(Long labelId)
    {
        LabelMerge  labelMerge=labelMergeMapper.findLabelMergeById(labelId);
        return BeanConvertor.convertBean(labelMerge,LabelMergeDTO.class);
    }


    /**
     * 查询内容与标签关联列表
     *
     * @param labelMerge 内容与标签关联
     * @return 内容与标签关联
     */
    @Override
    public Pager<LabelMergeDTO> selectLabelMergePager(LabelMerge labelMerge, Integer page, Integer limit, String sort, String order)
    {
        Pager<LabelMergeDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(labelMerge);

        List<LabelMerge> labelMergeList=labelMergeMapper.selectLabelMergePager(pager);
        List<LabelMergeDTO> dtolsit = new ArrayList<LabelMergeDTO>();
        labelMergeList.forEach(entity -> {
            LabelMergeDTO dto = new LabelMergeDTO();
            dto.setLabelId(entity.getLabelId());
            dto.setInfoId(entity.getInfoId());
            dto.setInfoType(entity.getInfoType());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(labelMergeMapper.queryLabelMergeTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的内容与标签关联列表
     *
     * @param labelMerge 内容与标签关联
     * @return 内容与标签关联集合
     */
    @Override
    public List<LabelMergeDTO> exportLabelMergeList(LabelMerge labelMerge) {
        return BeanConvertor.copyList(labelMergeMapper.exportLabelMergeList(labelMerge),LabelMergeDTO.class);
    }
}
