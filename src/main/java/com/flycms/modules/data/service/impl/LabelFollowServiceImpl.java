package com.flycms.modules.data.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.mapper.LabelFollowMapper;
import com.flycms.modules.data.domain.LabelFollow;
import com.flycms.modules.data.domain.dto.LabelFollowDTO;
import com.flycms.modules.data.service.ILabelFollowService;

import java.util.ArrayList;
import java.util.List;

/**
 * 话题关注Service业务层处理
 * 
 * @author admin
 * @date 2021-02-01
 */
@Service
public class LabelFollowServiceImpl implements ILabelFollowService 
{
    @Autowired
    private LabelFollowMapper labelFollowMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题关注
     *
     * @param labelFollow 话题关注
     * @return 结果
     */
    @Override
    public int insertLabelFollow(LabelFollow labelFollow)
    {
        labelFollow.setId(SnowFlakeUtils.nextId());
        labelFollow.setCreateTime(DateUtils.getNowDate());
        return labelFollowMapper.insertLabelFollow(labelFollow);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////

    /**
     * 删除话题关注信息
     *
     * @param id 话题关注ID
     * @return 结果
     */
    @Override
    public int deleteLabelFollowById(Long id)
    {
        return labelFollowMapper.deleteLabelFollowById(id);
    }

    /**
     * 批量删除话题关注
     *
     * @param ids 需要删除的话题关注ID
     * @return 结果
     */
    @Override
    public int deleteLabelFollowByIds(Long[] ids)
    {
        return labelFollowMapper.deleteLabelFollowByIds(ids);
    }

    /**
     * 删除用户和标签关联
     *
     * @param labelId 标签id
     * @param userId 用户id
     * @return 结果
     */
    @Override
    public int deleteLabelFollow(Long labelId,Long userId)
    {
        return labelFollowMapper.deleteLabelFollow(labelId,userId);
    }
    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题关注
     *
     * @param labelFollow 话题关注
     * @return 结果
     */
    @Override
    public int updateLabelFollow(LabelFollow labelFollow)
    {
        return labelFollowMapper.updateLabelFollow(labelFollow);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     *
     * 检查是否已关注该标签
     *
     * @param labelId 标签id
     * @param userId 用户id
     * @return
     */
    @Override
    public boolean checkLabelFollow(Long labelId,Long userId) {
        int totalCount = labelFollowMapper.checkLabelFollow(labelId,userId);
        return totalCount > 0 ? true : false;
    }

    /**
     * 查询话题关注
     * 
     * @param id 话题关注ID
     * @return 话题关注
     */
    @Override
    public LabelFollowDTO findLabelFollowById(Long id)
    {
        LabelFollow labelFollow=labelFollowMapper.findLabelFollowById(id);
        return BeanConvertor.convertBean(labelFollow,LabelFollowDTO.class);
    }


    /**
     * 查询话题关注列表
     *
     * @param labelFollow 话题关注
     * @return 话题关注
     */
    @Override
    public Pager<LabelFollowDTO> selectLabelFollowPager(LabelFollow labelFollow, Integer page, Integer limit, String sort, String order)
    {
        Pager<LabelFollowDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(labelFollow);

        List<LabelFollow> labelFollowList=labelFollowMapper.selectLabelFollowPager(pager);
        List<LabelFollowDTO> dtolsit = new ArrayList<LabelFollowDTO>();
        labelFollowList.forEach(entity -> {
            LabelFollowDTO dto = new LabelFollowDTO();
            dto.setId(entity.getId());
            dto.setLabelId(entity.getLabelId());
            dto.setUserId(entity.getUserId());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(labelFollowMapper.queryLabelFollowTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的话题关注列表
     *
     * @param labelFollow 话题关注
     * @return 话题关注集合
     */
    @Override
    public List<LabelFollowDTO> exportLabelFollowList(LabelFollow labelFollow) {
        return BeanConvertor.copyList(labelFollowMapper.exportLabelFollowList(labelFollow),LabelFollowDTO.class);
    }
}
