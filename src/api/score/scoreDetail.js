import request from '@/utils/request'

// 查询积分日志列表
export function listScoreDetail(query) {
  return request({
    url: '/system/score/scoreDetail/list',
    method: 'get',
    params: query
  })
}

// 查询积分日志详细
export function getScoreDetail(id) {
  return request({
    url: '/system/score/scoreDetail/' + id,
    method: 'get'
  })
}

// 新增积分日志
export function addScoreDetail(data) {
  return request({
    url: '/system/score/scoreDetail',
    method: 'post',
    data: data
  })
}

// 修改积分日志
export function updateScoreDetail(data) {
  return request({
    url: '/system/score/scoreDetail',
    method: 'put',
    data: data
  })
}

// 删除积分日志
export function delScoreDetail(id) {
  return request({
    url: '/system/score/scoreDetail/' + id,
    method: 'delete'
  })
}

// 导出积分日志
export function exportScoreDetail(query) {
  return request({
    url: '/system/score/scoreDetail/export',
    method: 'get',
    params: query
  })
}