import request from '@/utils/request'

// 索引全部
export function indexTopic() {
  return request({
    url: '/system/group/topic/all-index',
    method: 'get'
  })
}
