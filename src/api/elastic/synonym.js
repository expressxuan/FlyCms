import request from '@/utils/request'

// 查询同义词词库列表
export function listSynonym(query) {
  return request({
    url: '/system/elastic/synonym/list',
    method: 'get',
    params: query
  })
}

// 查询同义词词库详细
export function getSynonym(id) {
  return request({
    url: '/system/elastic/synonym/' + id,
    method: 'get'
  })
}

// 新增同义词词库
export function addSynonym(data) {
  return request({
    url: '/system/elastic/synonym',
    method: 'post',
    data: data
  })
}

// 修改同义词词库
export function updateSynonym(data) {
  return request({
    url: '/system/elastic/synonym',
    method: 'put',
    data: data
  })
}

// 删除同义词词库
export function delSynonym(id) {
  return request({
    url: '/system/elastic/synonym/' + id,
    method: 'delete'
  })
}

// 导出同义词词库
export function exportSynonym(query) {
  return request({
    url: '/system/elastic/synonym/export',
    method: 'get',
    params: query
  })
}