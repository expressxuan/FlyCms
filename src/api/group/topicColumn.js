import request from '@/utils/request'

// 查询帖子分类列表
export function listTopicColumn(query) {
  return request({
    url: '/system/group/topicColumn/list',
    method: 'get',
    params: query
  })
}

// 查询帖子分类详细
export function getTopicColumn(id) {
  return request({
    url: '/system/group/topicColumn/' + id,
    method: 'get'
  })
}

// 新增帖子分类
export function addTopicColumn(data) {
  return request({
    url: '/system/group/topicColumn',
    method: 'post',
    data: data
  })
}

// 修改帖子分类
export function updateTopicColumn(data) {
  return request({
    url: '/system/group/topicColumn',
    method: 'put',
    data: data
  })
}

// 删除帖子分类
export function delTopicColumn(id) {
  return request({
    url: '/system/group/topicColumn/' + id,
    method: 'delete'
  })
}

// 导出帖子分类
export function exportTopicColumn(query) {
  return request({
    url: '/system/group/topicColumn/export',
    method: 'get',
    params: query
  })
}