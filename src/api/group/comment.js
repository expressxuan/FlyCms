import request from '@/utils/request'

// 查询话题回复/评论列表
export function listComment(query) {
  return request({
    url: '/system/group/comment/list',
    method: 'get',
    params: query
  })
}

// 查询话题回复/评论详细
export function getComment(id) {
  return request({
    url: '/system/group/comment/' + id,
    method: 'get'
  })
}

// 修改话题回复/评论
export function updateComment(data) {
  return request({
    url: '/system/group/comment',
    method: 'put',
    data: data
  })
}

// 删除话题回复/评论
export function delComment(id) {
  return request({
    url: '/system/group/comment/' + id,
    method: 'delete'
  })
}

// 导出话题回复/评论
export function exportComment(query) {
  return request({
    url: '/system/group/comment/export',
    method: 'get',
    params: query
  })
}