import request from '@/utils/request'

// 查询小组话题列表
export function listTopic(query) {
  return request({
    url: '/system/group/topic/list',
    method: 'get',
    params: query
  })
}

// 查询小组话题详细
export function getTopic(id) {
  return request({
    url: '/system/group/topic/' + id,
    method: 'get'
  })
}

// 新增小组话题
export function addTopic(data) {
  return request({
    url: '/system/group/topic',
    method: 'post',
    data: data
  })
}

// 修改小组话题
export function updateTopic(data) {
  return request({
    url: '/system/group/topic',
    method: 'put',
    data: data
  })
}

// 删除小组话题
export function delTopic(id) {
  return request({
    url: '/system/group/topic/' + id,
    method: 'delete'
  })
}

// 导出小组话题
export function exportTopic(query) {
  return request({
    url: '/system/group/topic/export',
    method: 'get',
    params: query
  })
}

// 获取岗位选择框列表
export function optionselect() {
  return request({
    url: '/system/post/optionselect',
    method: 'get'
  })
}
//获取七牛token
export function qiniuToken() {
  return request({
    url: '/system/qiniu/auth',
    method: 'get'
  })
}

export function upload(data) {
  return request({
    url: '/system/qiniu/upload',
    method: 'post',
    data:data
  })
}