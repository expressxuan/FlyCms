import request from '@/utils/request'

// 查询小组帖子专辑列表
export function listAlbum(query) {
  return request({
    url: '/system/group/album/list',
    method: 'get',
    params: query
  })
}

// 查询小组帖子专辑详细
export function getAlbum(id) {
  return request({
    url: '/system/group/album/' + id,
    method: 'get'
  })
}

// 新增小组帖子专辑
export function addAlbum(data) {
  return request({
    url: '/system/group/album',
    method: 'post',
    data: data
  })
}

// 修改小组帖子专辑
export function updateAlbum(data) {
  return request({
    url: '/system/group/album',
    method: 'put',
    data: data
  })
}

// 删除小组帖子专辑
export function delAlbum(id) {
  return request({
    url: '/system/group/album/' + id,
    method: 'delete'
  })
}

// 导出小组帖子专辑
export function exportAlbum(query) {
  return request({
    url: '/system/group/album/export',
    method: 'get',
    params: query
  })
}