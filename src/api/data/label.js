import request from '@/utils/request'

// 查询标签列表
export function listLabel(query) {
  return request({
    url: '/system/data/label/list',
    method: 'get',
    params: query
  })
}

// 查询标签详细
export function getLabel(id) {
  return request({
    url: '/system/data/label/' + id,
    method: 'get'
  })
}

// 新增标签
export function addLabel(data) {
  return request({
    url: '/system/data/label',
    method: 'post',
    data: data
  })
}

// 修改标签
export function updateLabel(data) {
  return request({
    url: '/system/data/label',
    method: 'put',
    data: data
  })
}

// 删除标签
export function delLabel(id) {
  return request({
    url: '/system/data/label/' + id,
    method: 'delete'
  })
}

// 导出标签
export function exportLabel(query) {
  return request({
    url: '/system/data/label/export',
    method: 'get',
    params: query
  })
}