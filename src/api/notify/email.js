import request from '@/utils/request'

// 查询邮件服务器列表
export function listEmail(query) {
  return request({
    url: '/system/notify/email/list',
    method: 'get',
    params: query
  })
}

// 查询邮件服务器详细
export function getEmail(mailServer) {
  return request({
    url: '/system/notify/email/' + mailServer,
    method: 'get'
  })
}

// 新增邮件服务器
export function addEmail(data) {
  return request({
    url: '/system/notify/email',
    method: 'post',
    data: data
  })
}

// 修改邮件服务器
export function updateEmail(data) {
  return request({
    url: '/system/notify/email',
    method: 'put',
    data: data
  })
}

// 删除邮件服务器
export function delEmail(mailServer) {
  return request({
    url: '/system/notify/email/' + mailServer,
    method: 'delete'
  })
}

// 导出邮件服务器
export function exportEmail(query) {
  return request({
    url: '/system/notify/email/export',
    method: 'get',
    params: query
  })
}