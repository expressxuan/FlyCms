import request from '@/utils/request'

// 查询短信签名列表
export function listSmsSign(query) {
  return request({
    url: '/system/notify/SmsSign/list',
    method: 'get',
    params: query
  })
}

// 查询短信签名详细
export function getSmsSign(id) {
  return request({
    url: '/system/notify/SmsSign/' + id,
    method: 'get'
  })
}

// 新增短信签名
export function addSmsSign(data) {
  return request({
    url: '/system/notify/SmsSign',
    method: 'post',
    data: data
  })
}

// 修改短信签名
export function updateSmsSign(data) {
  return request({
    url: '/system/notify/SmsSign',
    method: 'put',
    data: data
  })
}

// 删除短信签名
export function delSmsSign(id) {
  return request({
    url: '/system/notify/SmsSign/' + id,
    method: 'delete'
  })
}

// 导出短信签名
export function exportSmsSign(query) {
  return request({
    url: '/system/notify/SmsSign/export',
    method: 'get',
    params: query
  })
}