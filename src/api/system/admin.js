import request from '@/utils/request'
import { praseStrEmpty } from "@/utils/flymall";

// 查询用户列表
export function listAdmin(query) {
  return request({
    url: '/system/admin/list',
    method: 'get',
    params: query
  })
}

// 查询用户名称列表
export function listAdminName(adminName) {
  return request({
    url: '/system/admin/query_list/',
    method: 'get',
    params: {adminName : adminName}
  })
}

// 查询用户详细
export function getAdmin(adminId) {
  return request({
    url: '/system/admin/' + praseStrEmpty(adminId),
    method: 'get'
  })
}

// 新增用户
export function addAdmin(data) {
  return request({
    url: '/system/admin',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateAdmin(data) {
  return request({
    url: '/system/admin',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delAdmin(adminId) {
  return request({
    url: '/system/admin/' + adminId,
    method: 'delete'
  })
}

// 导出用户
export function exportAdmin(query) {
  return request({
    url: '/system/admin/export',
    method: 'get',
    params: query
  })
}

// 用户密码重置
export function resetAdminPwd(adminId, password) {
  const data = {
    adminId,
    password
  }
  return request({
    url: '/system/admin/resetPwd',
    method: 'put',
    data: data
  })
}

// 用户状态修改
export function changeAdminStatus(adminId, status) {
  const data = {
    adminId,
    status
  }
  return request({
    url: '/system/admin/changeStatus',
    method: 'put',
    data: data
  })
}

// 查询用户个人信息
export function getAdminProfile() {
  return request({
    url: '/system/admin/profile',
    method: 'get'
  })
}

// 修改用户个人信息
export function updateAdminProfile(data) {
  return request({
    url: '/system/admin/profile',
    method: 'put',
    data: data
  })
}

// 用户密码重置
export function updateAdminPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return request({
    url: '/system/admin/profile/updatePwd',
    method: 'put',
    params: data
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/system/admin/profile/avatar',
    method: 'post',
    data: data
  })
}

// 下载用户导入模板
export function importTemplate() {
  return request({
    url: '/system/admin/importTemplate',
    method: 'get'
  })
}
