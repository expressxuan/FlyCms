import request from '@/utils/request'

// 查询素材列表
export function listSource(query) {
  return request({
    url: '/system/cms/source/list',
    method: 'get',
    params: query
  })
}

// 查询素材详细
export function getSource(id) {
  return request({
    url: '/system/cms/source/' + id,
    method: 'get'
  })
}

// 新增素材
export function addSource(data) {
  return request({
    url: '/system/cms/source',
    method: 'post',
    data: data
  })
}

// 修改素材
export function updateSource(data) {
  return request({
    url: '/system/cms/source',
    method: 'put',
    data: data
  })
}

// 删除素材
export function delSource(id) {
  return request({
    url: '/system/cms/source/' + id,
    method: 'delete'
  })
}

// 导出素材
export function exportSource(query) {
  return request({
    url: '/system/cms/source/export',
    method: 'get',
    params: query
  })
}