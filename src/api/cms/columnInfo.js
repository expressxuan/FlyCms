import request from '@/utils/request'

// 查询栏目列表
export function listColumnInfo(query) {
  return request({
    url: '/system/cms/columnInfo/list2',
    method: 'get',
    params: query
  })
}

// 查询栏目详细
export function getColumnInfo(id) {
  return request({
    url: '/system/cms/columnInfo/' + id,
    method: 'get'
  })
}

// 新增栏目
export function addColumnInfo(data) {
  return request({
    url: '/system/cms/columnInfo',
    method: 'post',
    data: data
  })
}

// 修改栏目
export function updateColumnInfo(data) {
  return request({
    url: '/system/cms/columnInfo',
    method: 'put',
    data: data
  })
}

// 删除栏目
export function delColumnInfo(id) {
  return request({
    url: '/system/cms/columnInfo/' + id,
    method: 'delete'
  })
}

// 导出栏目
export function exportColumnInfo(query) {
  return request({
    url: '/system/cms/columnInfo/export',
    method: 'get',
    params: query
  })
}