import request from '@/utils/request'

// 查询文章列表
export function listArticle(lastColumnInfoId) {
  return request({
    url: `/system/cms/article/list?lastColumnInfoId=${lastColumnInfoId}`,
    method: 'get',
  })
}
// 查询文章标题
export function titleArticle(title) {
  return request({
    url: `/system/cms/article/list?title=${title}`,
    method: 'get',
  })
}
// 查询文章详细
export function getArticle(id) {
  return request({
    url: '/system/cms/article/' + id,
    method: 'get'
  })
}

// 新增文章
export function addArticle(data) {
  return request({
    url: '/system/cms/article',
    method: 'post',
    data: data
  })
}

// 修改文章
export function updateArticle(data) {
  return request({
    url: '/system/cms/article',
    method: 'put',
    data: data
  })
}

// 删除文章
export function delArticle(id) {
  return request({
    url: '/system/cms/article/' + id,
    method: 'delete'
  })
}

// 导出文章
export function exportArticle(query) {
  return request({
    url: '/system/cms/article/export',
    method: 'get',
    params: query
  })
}