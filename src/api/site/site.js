import request from '@/utils/request'

// 查询官网设置详细
export function getSite() {
  return request({
    url: '/system/site/query',
    method: 'get'
  })
}


// 修改官网设置
export function updateSite(data) {
  return request({
    url: '/system/site/edit',
    method: 'put',
    data: data
  })
}
