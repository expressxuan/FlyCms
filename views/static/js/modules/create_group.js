$(document).ready(function(){
    $(document).on('click', '#comm-submit', function (){
        var groupName=$("#groupName").val();
        var groupDesc=$("#groupDesc").val();
        var tag=$("#tag").val();
        $.ajax({
            url:'/user/my/group/create',
            type:'post',
            data:{'groupName':groupName,'groupDesc':groupDesc,"tag":tag},
            dataType:'json',
            success:function(data){
                if(data.code==200){
                    layer.msg("添加成功", { shift: -1 }, function () {
                        location.href = "/user/my/group/create";
                    });
                }else{
                    alert(data.msg);
                }
            },
            error:function(){
                console.log('请求出错！');
            }
        })
    });
});